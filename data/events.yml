- topic: DevOps Enterprise Summit US
  type: Conference
  date: Oct 22-24, 2018
  date_ends: October 25, 2018 # Month DD, YYYY
  description: |
    DevOps Enterprise Summit is a conference for the leaders of large, complex organizations implementing DevOps principles and practices. The event programming emphasizes both evolving technical and architectural practices and the methods needed to lead widespread change efforts in large organizations. The goal is to give leaders the tools and practices they need to develop and deploy software faster and to win in the marketplace.
  location: Las Vegas, NV, USA
  region: NORAM
  social_tags: DevOps, DOES
  event_url: https://about.gitlab.com/events/does-us/
  url: does-us
  header_background: /images/events/meeting_image.jpg
  header_image: /images/events/does.svg
  header_description: "Drop by our booth or schedule a time, we'd love to chat!"
  booth: 519
  form:
      title: "Schedule time to chat"
      description: "Learn more about how GitLab can simplify toolchain complexity and speeds up cycle times."
      number: 1691
      success_message: "Thank you for registering!"
  content:  |
      ### How you can get started with GitLab and Auto DevOps

      * [Auto DevOps with GitLab](/ee/topics/autodevops/)
      * [Top Five Cloud Trends](/2018/08/02/top-five-cloud-trends/)
      * [How Jaguar Land Rover embraced CI to speed up their software lifecycle](/2018/07/23/chris-hill-devops-enterprise-summit-talk/)

- topic: AWS re:Invent
  type: Conference, Speaking Engagement
  date: November 26-30, 2018
  date_ends: December 1, 2018 # Month DD, YYYY
  description: |
               AWS re:Invent 2018 is the Amazon Web Services annual user conference dedicated to cloud strategies, IT architecture and infrastructure, operations, security and developer productivity.
  location: Las Vegas, NV USA
  region: NORAM
  social_tags: AWSreInvent2018
  event_url: https://about.gitlab.com/events/aws-reinvent/
  # Giving the following data will give this event it's own dedicated page on about.gitlab.com, must provide a unique url.
  # If it is text, it needs to be wrapped in "double quotes". This is so you can use characters like : and ' without breaking anything.
  url: aws-reinvent
  header_background: /images/events/IMG_4756.jpeg
  header_image: /images/events/aws-reinvent.svg
  header_description: "Drop by our booth or schedule a time, we are great listeners!"
  booth: 2608
  content:  |
      ### Let's Meet!

      Join us for a live demo on getting started with Auto DevOps on Nov 28th at 1pm to learn how Auto DevOps simplifies your deployment pipeline to accelerate delivery by 200%, improves responsiveness, and closes the feedback gap between you and your users.

      ### How you can get started with GitLab and AWS

      * [GitLab + AWS](/solutions/aws/)
        * Learn more about how GitLab and AWS work together.
      * [Simple Deployment to Amazon EKS](/2018/06/06/eks-gitlab-integration/)
        * Amazon EKS is now GA! We’ve partnered with AWS to make sure GitLab support is available out of the gate. Here’s how you can take advantage.
      * [Top Five Cloud Trends](/2018/08/02/top-five-cloud-trends/)
        * Cloud computing is officially where it's at. Find out who's in the lead and how to plan for the future.
      * [How Jaguar Land Rover embraced CI to speed up their software lifecycle](/2018/07/23/chris-hill-devops-enterprise-summit-talk/)
        * Inspiration, persistence, an attitude of continuous improvement – how adopting CI helped this vehicle company implement software over the air.



  speakers:
    - name: "Sid Sijbrandij"
      title: "CEO"
      image: /images/team/picture_sytse-crop.jpg
      date: "Tuesday, Nov 27th"
      time: "12:10pm PT"
      location: "Pilvi Theater"
      topic: "Reinvent your pipeline with GitLab, Kubernetes, and Amazon EKS"
      description: "See how we automated CI/CD configuration based on learning from 2000+ contributors and 100,000+ organizations using GitLab. In the enterprise, getting deployment pipelines set up can take weeks. At GitLab we wanted to get developers started in minutes and wanted to build an experience where the only thing needed to get live in production is to commit your code- GitLab does the rest. Learn how we integrated Kubernetes with our auto-configuration capabilities to make setup as simple as just a few clicks. Watch our live demo on Amazon EKS of how fast you can go from code to production."
    - name: "Josh Lambert"
      title: "Senior Product Manager, Monitor"
      image: /images/team/joshua-crop.jpg
      date: "Wednesday, Nov 28"
      time: "11:30am PT"
      location: "Booth 2608 at the expo floor in the Venitian"
      topic: "AutoDevops in 5 min"
      description: "Auto DevOps is a fully featured CI/CD pipeline that automates the delivery process. Simply commit your code and Auto DevOps does the rest. Find out how you can easily get started."
    - name: "Reb"
      title: "Solutions Architect"
      image: /images/team/reb-crop.jpg
      date: "Tuesday, Nov 27"
      time: "3:30pm PT"
      location: "Booth 2608"
      topic: "GitLab CI 101"
      description: "In this talk, we will review how simple it is to get started with GitLab's built in CI tool."
    - name: "Demos Every Day"
      title: "Solutions Architects"
      date: "Daily"
      time: "11:30 and 3:30pm PT"
      location: "Booth 2608"
      topic: "Getting Started with GitLab on AWS"
      description: "Deploy GitLab on AWS in minutes"

- topic: GitLab Hackathon
  type: Community Event
  date: November 14-15, 2018
  date_ends: November 15, 2018
  description: |
      GitLab Hackathon is a virtual event that takes place once a quarter where community members get together to work on merge requests, participate in tutorial sessions, and also to help new community members. The event is open to anyone who are interested in contributing code, documentation, transations, UX design, etc. to GitLab.
  location: Gitter
  region: Online
  social_tags: gitlab, hackathon, gitlabhackathon
  event_url: /community/hackathon/

- topic: StartCon
  type: Conference
  date: November 30, 2018
  date_ends: December 1, 2018
  location: Royal Randwick Racecourse, Sydney, Australia
  region: APAC
  event_url: https://www.startcon.com/

- topic: Evanta CISO Boardroom
  type: Conference
  date: December 3, 2018
  date_ends: December 3, 2018
  description: |
               With an agenda built entirely “by CISOs, for CISOs,” the San Francisco CISO Executive Summit provides a platform for us to develop tangible solutions to the biggest challenges facing our community.
  location: Intercontinental Hotel, San Francisco, CA
  region: NORAM
  social_tags: Evanta
  event_url: https://www.evanta.com/ciso

- topic: devopsdays Charlotte
  type: Conference
  date: February 7-8, 2019
  date_ends: February 8, 2019
  description: |
               From startups to the big banks, Charlotte is home to an awesome tech community. We’re proud to bring back DevOpsDays Charlotte for the fourth time! DevOpsDays Charlotte will bring 250+ development, operations, security, and management professionals together to discuss the culture, processes, and tools to enable better organizations and innovative products.
  region: NORAM
  social_tags: devopsdaysCharlotte
  event_url: https://www.devopsdays.org/events/2019-charlotte/welcome/

- topic: FinServices Roundtable Dinner
  type: MeetUp
  date: December 04, 2018
  date_ends: December 04, 2018 # Month DD, YYYY
  description: |
               Join GitLab and your local Financial Services peers for an evening discussing the current state of your DevOps transformation.
  location: The Capital Grille, New York, NY
  region: NORAM
  url: finservices-dinner-nyc
  header_background: /images/events/finservice-dinner.jpg
  header_description: "Presented by GitLab"
  content: |
      ### The Calloway Room | 6:00 pm to 8:30 pm

      Join GitLab and your local Financial Services peers for an evening discussing the current state of your DevOps transformation, sharing where you and your organization have had successes, and opportunities where you'd like help from your colleagues.

      We will kick off the event promptly at 6:00 pm in the Calloway Room. Space is limited. Please register to hold your spot. Registration is free and dinner is included.

- topic: GitLab product demo and live Q&A
  type: Webcast
  date: December 5, 2018
  date_ends: December 5, 2018
  description: |
    Join us for a 30-minute session where a product marketing manager will demo a change through the entire DevOps lifecycle using GitLab from Planning to Monitoring, with a live Q&A!
  location: Webcast, Zoom
  region: Online
  social_tags: WeeklyDemo
  event_url: https://about.gitlab.com/webcast/weekly-demo/

- topic: Evanta CXO Boardroom
  type: Conference
  date: December 12, 2018
  date_ends: December 12, 2018
  description: |
               With an agenda built entirely “by CIOs, for CIOs,” the Seattle CIO Executive Summit provides a platform for us to develop tangible solutions to the biggest challenges facing our community.
  location: Grand Hyatt Seattle, WA
  region: NORAM
  social_tags: Evanta
  event_url: https://www.evanta.com/cio

- topic: GitLab product demo and live Q&A
  type: Webcast
  date: December 12, 2018
  date_ends: December 12, 2018
  description: |
    Join us for a 30-minute session where a product marketing manager will demo a change through the entire DevOps lifecycle using GitLab from Planning to Monitoring, with a live Q&A!
  location: Webcast, Zoom
  region: Online
  social_tags: WeeklyDemo
  event_url: https://about.gitlab.com/webcast/weekly-demo/

- topic: KubeCon Cloud Native Con NA
  type: Conference, Speaking Engagement
  date: December 11-13, 2018
  date_ends: December 13, 2018
  description: |
         The Cloud Native Computing Foundation’s flagship conference gathers adopters and technologists from leading open source and cloud native communities in Seattle, WA on December 11-13, 2018. Join Kubernetes, Prometheus, OpenTracing, Fluentd, gRPC, containerd, rkt, CNI, Envoy, Jaeger, Notary, TUF, Vitess, CoreDNS, NATS, and Linkerd as the community gathers for three days to further the education and advancement of cloud native computing. This year we will have 3 Gitlabber's speaking at the event. [Monolith to Microservice: Pitchforks Not Included](https://kccna18.sched.com/event/GrSP?iframe=no) with Jason Plum. [Using Application Identity to Correlate Metrics: A look at SPIFFE and SPIRE](https://kccna18.sched.com/event/GrZW) with Priyanka Sharma. [Becoming Cloud Native Without Starting From Scratch](https://kccna18.sched.com/event/GrRC) with Marin Jankovski.
  location: Seattle, WA USA
  region: NORAM
  social_tags: KubeCon
  event_url: https://events.linuxfoundation.org/events/kubecon-cloudnativecon-north-america-2018/
  # Giving the following data will give this event it's own dedicated page on about.gitlab.com, must provide a unique url.
  # If it is text, it needs to be wrapped in "double quotes". This is so you can use characters like : and ' without breaking anything.
  url: kubecon-us
  header_background: /images/events/neon_sign.jpeg
  header_image: /images/events/kccnc-na-color.png
  header_description: "Drop by our booth or schedule a time for a demo, we are great listeners!"
  booth: "S44"
  content:  |

      ### Events at Kubecon

      ![Kubecon Ice Cube event png](/images/events/kubecon-ice-cube.png){: .margin-bottom10 .margin-top40}

      <h4>Ice Cube Concert w/ Mesosphere</h4>

      Tuesday December 11, 6:30-9:00pm

      [Get tickets](https://www.icecubecon.com/)

      ![Kubecon happy hour Mesosphere event png](/images/events/kubecon-meso-gcloud.png){: .margin-bottom10 .margin-top40}

      <h4>VIP Happy Hour Hosted by Mesosphere & Google Cloud</h4>

      Wednesday December 12, 5:00-8:00pm

      [Get tickets](https://www.eventbrite.com/e/kubecon-vip-happy-hour-hosted-by-mesosphere-sponsored-in-part-by-google-cloud-tickets-52099696595?utm_email=organic&utm_campaign=partner-gitlab&utm_source=mesosphere)

      ![Kubecon Speakeasy event png](/images/events/kubecon-speakeasy.png){: .margin-bottom10 .margin-top40}

      <h4>Speakeasy KubeCon w/ Upbound</h4>

      Monday December 10, 7:30pm-Midnight

      [Get tickets](https://www.eventbrite.com/e/the-kubecon-kickoff-speakeasy-tickets-52699487586)

      <br>
      <br>

      ### How you can get started with GitLab and Kubernetes

      * [Connecting GitLab with a Kubernetes cluster](https://docs.gitlab.com/ee/user/project/clusters/)
        * Connect your project to Google Kubernetes Engine (GKE) or an existing Kubernetes cluster in a few steps.
      * [Installing GitLab on Kubernetes](https://docs.gitlab.com/ee/install/kubernetes/)
        * The easiest method to deploy GitLab on Kubernetes is to take advantage of GitLab’s Helm charts.
      * [GitLab + Kubernetes](https://about.gitlab.com/solutions/kubernetes/)
        * Everything you need to build, test, deploy, and run your app at scale
      * [GitLab Prometheus](https://docs.gitlab.com/ee/administration/monitoring/prometheus/)
        * Prometheus is a powerful time-series monitoring service, providing a flexible platform for monitoring GitLab and other software products. GitLab provides out of the box monitoring with Prometheus, providing easy access to high quality time-series monitoring of GitLab services.
      * [Why Prometheus is for everyone](https://about.gitlab.com/2018/09/27/why-all-organizations-need-prometheus/)
        * It's no secret that here at GitLab, we hitched our wagon to Prometheus long ago. We've been shipping it with GitLab since 8.16. Here’s why your organization should be using it too
      * [How Jaguar Land Rover embraced CI to speed up their software lifecycle](/2018/07/23/chris-hill-devops-enterprise-summit-talk/)
        * Inspiration, persistence, an attitude of continuous improvement – how adopting CI helped this vehicle company implement software over the air.


  speakers:
    - name: "Priyanka Sharma"
      title: "Using Application Identity to Correlate Metrics: A look at SPIFFE and SPIRE"
      image: /images/team/priyankasharma-crop.jpg
      date: "Wednesday, Dec 12th"
      time: "1:45pm - 2:20pm PT"
      location: "Tahoma 3/4 @ TCC The Conference Center (TCC)"
      topic: "Using Application Identity to Correlate Metrics: A look at SPIFFE and SPIRE"
      description: "In an ideal world, we would have a standardized way to identify running software systems that our monitoring tools could easily lean on, even when spread over multiple teams, geographies, and platforms. But real-world deployments are rarely so simple. I will explain how application identity can be used as the basis for correlating metrics from multiple sources (with the help of OpenTracing) and detail some of the challenges inherent in defining application identity in different contexts (such as virtual machines, functions, and different Kubernetes primitives). I then offer an overview of open source projects like SPIFFE and SPIRE, which have modernized identity authentication across microservices, and demonstrates how SPIRE, Fluentd, Prometheus, and Jaeger can be used together to precisely correlate logs, metrics, and traces to improve and diagnose real-world production issues."
    - name: "Marin Jankovski"
      title: "Engineering Manager, Distribution & Delivery"
      image: /images/team/marin-crop.jpg
      date: "Tuesday, Dec 11"
      time: "4:30pm - 5:05pm PT"
      location: "6E"
      topic: "Becoming Cloud Native Without Starting From Scratch"
      description: "Full rewrite of a working application is a luxury most companies can't afford. In this session, we will talk about how running GitLab architecture was adapted to be closer to a regular cloud native application without having to rewrite the whole application and disrupt ongoing product development. As an additional requirement, architecture had to be modeled on what is required for installation of GitLab.com scale (SaaS) while also keeping in mind on-premises installation customers which will use the same set of tools. We will discuss what parts of the application we left out of Kubernetes, how we loosely decoupled previously integrated components and how and why are we enforcing some old behaviours. We will share our experiences with using de-facto standard for distributing applications (Helm), but some mis-steps and some good choices we think we made."
    - name: "Jason Plum"
      title: "Senior Distribution Engineer- Kubernetes Expert"
      image: /images/team/jplum-crop.jpg
      date: "Thursday, December 13"
      time: "2:35pm - 3:10pm PT"
      location: "Ballroom 6B"
      topic: "Monolith to Microservice: Pitchforks Not Included"
      description: "Learn how GitLab turned it’s omnibus into cloud native Helm charts by way of containerization and orchestration. This talk aims to help practitioners already running large scale, successful products make decisions on how to move to microservices while maintaining product development cadence and serving customers on legacy software everyday. It’s like driving a race car and fixing it as you are competing in a race, without pit stops."

- topic: Django Girls Sherpur
  type: Diversity
  date: December 16, 2018
  date_ends: December 16, 2018
  description: |
      "The Django Girls foundation brings together women for a one-day workshop for beginners in effort to encourage and inspire their pursuit of Tech "
  location: Sherpur, Bangladesh
  region: APAC
  social_tags: GitLabDiversity

- topic: Predict 2019
  type: Webcast
  date: December 18, 2018
  date_ends: December 18, 2018
  description: |
    Join us on Dec 18 at Predict 2019 Virtual Summit. Predict 2019 will feature keynotes, panel discussions and speaker sessions forecasting what lies ahead for DevOps, security, containers and more in the next year. This virtual event will offer expert analysis and insight to help attendees prepare for 2019. This year we will have 2 GitLabbers speaking at the event!
  location: Webcast, Zoom
  region: Online
  social_tags: predict2019
  event_url: https://predict2019.com/

- topic: GitLab product demo and live Q&A
  type: Webcast
  date: January 9, 2019
  date_ends: January 9, 2019
  description: |
    Join us for a 30-minute session where a product marketing manager will demo a change through the entire DevOps lifecycle using GitLab from Planning to Monitoring, with a live Q&A!
  location: Webcast, Zoom
  region: Online
  social_tags: WeeklyDemo
  event_url: https://about.gitlab.com/webcast/weekly-demo/

- topic: GitLab product demo and live Q&A
  type: Webcast
  date: January 16, 2019
  date_ends: January 16, 2019
  description: |
    Join us for a 30-minute session where a product marketing manager will demo a change through the entire DevOps lifecycle using GitLab from Planning to Monitoring, with a live Q&A!
  location: Webcast, Zoom
  region: Online
  social_tags: WeeklyDemo
  event_url: https://about.gitlab.com/webcast/weekly-demo/

- topic: GitLab product demo and live Q&A
  type: Webcast
  date: January 23, 2019
  date_ends: January 23, 2019
  description: |
    Join us for a 30-minute session where a product marketing manager will demo a change through the entire DevOps lifecycle using GitLab from Planning to Monitoring, with a live Q&A!
  location: Webcast, Zoom
  region: Online
  social_tags: WeeklyDemo
  event_url: https://about.gitlab.com/webcast/weekly-demo/

- topic: GitLab product demo and live Q&A
  type: Webcast
  date: January 30, 2019
  date_ends: January 30, 2019
  description: |
    Join us for a 30-minute session where a product marketing manager will demo a change through the entire DevOps lifecycle using GitLab from Planning to Monitoring, with a live Q&A!
  location: Webcast, Zoom
  region: Online
  social_tags: WeeklyDemo
  event_url: https://about.gitlab.com/webcast/weekly-demo/

- topic: Git Merge 2019
  type: Conference
  date: Feb 1, 2019
  date_ends: February 1, 2019
  description: |
    The preeminent Git-focused conference. Git Merge is a full-day offering technical content and user case studies, plus a day of workshops for Git users of all levels.
  location: Brussels, Belgium
  region: EMEA
  social_tags: GitMerge
  event_url: https://git-merge.com/

- topic: FOSDEM 2019
  type: Conference
  date: February 2-3, 2019
  date_ends: February 3, 2019
  description: |
    FOSDEM is a free event for software developers to meet, share ideas and collaborate. Every year, thousands of developers of free and open source software from all over the world gather at the event in Brussels.
  location: Brussels, Belgium
  region: EMEA
  social_tags: fosdem
  event_url: https://fosdem.org/2019/

- topic: GitLab product demo and live Q&A
  type: Webcast
  date: February 6, 2019
  date_ends: February 6, 2019
  description: |
    Join us for a 30-minute session where a product marketing manager will demo a change through the entire DevOps lifecycle using GitLab from Planning to Monitoring, with a live Q&A!
  location: Webcast, Zoom
  region: Online
  social_tags: WeeklyDemo
  event_url: https://about.gitlab.com/webcast/weekly-demo/

- topic: GitLab product demo and live Q&A
  type: Webcast
  date: February 13, 2019
  date_ends: February 13, 2019
  description: |
    Join us for a 30-minute session where a product marketing manager will demo a change through the entire DevOps lifecycle using GitLab from Planning to Monitoring, with a live Q&A!
  location: Webcast, Zoom
  region: Online
  social_tags: WeeklyDemo
  event_url: https://about.gitlab.com/webcast/weekly-demo/

- topic: VueJS Amsterdam
  type: Conference
  date: February 14, 2019
  date_ends: February 15, 2019
  description: |
    Vuejs Amsterdam 2019 will be a two day conference for Vuejs enthusiasts from around the world. Vuejs Amsterdam was proud to host 1085 Attendees from 51 countries in 2018. If you never made it to Vuejs Amsterdam 2018, check out the aftermovie below. Don’t miss your chance to experience the “big screen” and extend your Vuejs knowledge.
  location: Amsterdam, Netherlands
  region: EMEA
  social_tags: vuelondon
  event_url: https://vuejs.london

- topic: Devopsdays New York City
  type: Conference
  date: January 24-25, 2019
  date_ends: January 25, 2019
  description: |
    devopsdays is a worldwide community conference series for anyone interested in IT improvement.
  location: New York City
  region: NORAM
  social_tags: devopsdaysNYC
  event_url: https://www.devopsdays.org/events/2019-new-york-city/welcome/

- topic: GitLab product demo and live Q&A
  type: Webcast
  date: February 20, 2019
  date_ends: February 20, 2019
  description: |
    Join us for a 30-minute session where a product marketing manager will demo a change through the entire DevOps lifecycle using GitLab from Planning to Monitoring, with a live Q&A!
  location: Webcast, Zoom
  region: Online
  social_tags: WeeklyDemo
  event_url: https://about.gitlab.com/webcast/weekly-demo/

- topic: GitLab product demo and live Q&A
  type: Webcast
  date: February 27, 2019
  date_ends: February 27, 2019
  description: |
    Join us for a 30-minute session where a product marketing manager will demo a change through the entire DevOps lifecycle using GitLab from Planning to Monitoring, with a live Q&A!
  location: Webcast, Zoom
  region: Online
  social_tags: WeeklyDemo
  event_url: https://about.gitlab.com/webcast/weekly-demo/

- topic: Open Source Leadership Summit
  type: Conference
  date: March 12-14, 2019
  date_ends: March 14, 2019
  description: |
    The Linux Foundation Open Source Leadership Summit is the premier forum where open source leaders convene to drive digital transformation with open source technologies and learn how to collaboratively manage the largest shared technology investment of our time.
  location: Half Moon Bay, CA
  region: NORAM
  social_tags: OpenSource
  event_url: https://events.linuxfoundation.org/events/open-source-leadership-summit-2019/

- topic: DrupalCon
  type: Conference, Speaking Engagement
  date: April 8-12, 2019
  date_ends: April 12, 2019
  description: |
    Join us at DrupalCon, the hub of the open web. GitLab moderators will be leading a panel discussion focused on large, independent open source projects.
  location: Seattle, WA
  region: NORAM
  social_tags: DrupalCon
  event_url: https://events.drupal.org/seattle2019

- topic: GitLab Contribute
  featured:
    background: /images/blogimages/nola.jpg
  type: Conference, Speaking Engagement
  date: May 8-14, 2019
  date_ends: May 14, 2019
  description: |
    Contribute to a product you want to work with, in a unique format. GitLab group summit.
  location: Hyatt Regency, New Orleans, LA
  region: NORAM
  social_tags: gitlabcontribute
  event_url: https://about.gitlab.com/events/gitlab-contribute/
  url: gitlab-contribute
  header_background: /images/blogimages/nola.jpg
  header_description: "Contribute to a product you want to work with, in a unique format"
  content:   |
    ### Who, What, Why

    <iframe width="140" height="79" src="https://www.youtube.com/embed/TExMuUjDg6I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


    **Why Contribute?**
    We love meeting people in the GitLab community. With that we mean anyone contributing to GitLab, with code, with content, with features or requests but also our customers, resellers, partners and our team's Significant Others.

    We would like to show you that your contribution is building something bigger and your voice and concerns are influential in what we do.

    We'd like to hear from you about experiences, preferences and knowledge, and build real world, useful products.

    All this comes back to our [GitLab Values](https://about.gitlab.com/values) of Collaboration, Results, Effectiveness, Diversity and Transparency.

    ### Still with us?
    [I want to contribute - Sign me up!](https://gitlab.com/gitlabcontribute/new-orleans/blob/master/README.md)

  speakers:
    - topic: "Tickets and Registration"
      description: "**Tickets are priced at $2499.** Once you arrive we will provide with all you will need, so you can just focus on getting the most out of your experience and contributions. We have special discounts for our team, their SO's and our community which will show at registration. [Details on all inclusions are on our project page](https://gitlab.com/gitlabcontribute/new-orleans/blob/master/README.md). We will keep updating that page with things like FAQ, tips and tricks from our team on how to make the best out your attendance, packinglists etc."
    - topic: "What to expect?"
      description: "This is not your average conference where you join sessions that we've set up for you and passively listen to the content. You are signing up to contribute. We would like to hear from you what topics you would like to discuss, and where your interests lie. More details on that are on the sign up form you will find later."
    - topic: We work <a href="https://remoteonly.org/">Remote-Only</a>
      description: "As you surely know, we are an all remote company with [over 390 team members in more than 45 countries worldwide](https://about.gitlab.com/team). Since we don't have watercooler talk or an office we also do a lot of fun stuff when we are all get together! Examples are playing Bubble Soccer in Austin, TX or Ziplining in Mexico. We visited Santorini in Greece and Table Mountain or Robben Island in Cape Town, South Africa. We would love for you to get to know us and each other, without a focus on your job or title. This way you will see the people that contribute to GitLab instead of just a name on your screen. We have a whole list of fun activities planned for New Orleans and are excited to spend time with you."
    - topic: "We don't…"
      description: "have different tracks, groups, special VIP content. We don't sit you down to listen to stale presentation after presentation about our product or features. You can find all that already in our [GitLab Handbook](https://about.gitlab.com/handbook)."
    - topic: "We do…"
      description: "want you to contribute to the topics of our workshops and [User Generated Content](URL) sessions during this week, which have ranged from discussing working remotely in an effective way, to pitching new features, to how to handle burnout."
    - topic: "We'll do something unique…"
      description: "We want to bring all our community and customers together and then leave the room. We'll give you time to truly hear how people work with our product, what is worth exploring and what is just a waste of time. Without us there, just honestly shared experiences."

- topic: AFCEA Defensive Cyber Operations Symposium
  type: Conference
  date: May 14 - 16, 2019
  date_ends: May 16, 2019
  description: |
               Cyber operations are a challenging mission for the U.S. Defense Department and government community that builds, operates and defends networks. Cyber leaders and warriors must continually evolve to adapt to future innovations and develop and leverage cutting-edge tools and technologies. Participants will discuss requirements and solutions to ensure that the networks within DoD are adaptive, resilient and effective across a range of uses and against diverse threats.
  location: Baltimore, MD USA
  region: NORAM
  social_tags: AFCEA
  event_url: https://events.afcea.org/AFCEACyberOps19/Public/enter.aspx

- topic: OSCON
  type: Conference
  date: July 15 - 18, 2019
  date_ends: July 18, 2019
  description: |
               OSCON brings open source community, enterprise, and engineering teams together to share best practices, projects that transform business, and insight into what you need next.
  location: Portland, OR USA
  region: NORAM
  social_tags: OSCON
  event_url: https://conferences.oreilly.com/oscon/oscon-or

- topic: Gartner Symposium
  type: Conference, Speaking Engagement
  date: Oct 14-18, 2019
  date_ends: Oct 18, 2019
  description: |
    Gartner Symposium/ITxpo 2018 is the place to hone your leadership skills, refine your strategies, and find the innovative technologies that will help to power your digital transformation.
  location: Orlando, FL
  region: NORAM
  social_tags: GartnerSymposium
  event_url: https://www.gartner.com/en/conferences/na/symposium-us
