---
layout: markdown_page
title: "Azure DevOps"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Positioning
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
On September 10, 2018 Microsoft renamed VSTS to Azure DevOps and by Q1 2019 will rename TFS to Azure DevOps Server, and upgrade both with the same new user interface.

Azure DevOps (VSTS) is a hosted cloud offering, and Azure DevOps Server (TFS), is an on-premises version. Both offer functionality that cover multiple stages of the DevOps lifecycle including planning tools, source code managment (SCM), and CI/CD. However, first development focus will be to Azure DevOps (SaaS). Their project manager shared that they are releasing on a 3-4 week pace. This seems evident based on their [published roadmap](https://docs.microsoft.com/en-us/azure/devops/release-notes/). The same project manager also shared that Azure DevOps Server (TFS) will be 3-4 months behind on adopting new features (also evident by their published roadmap). They are both from the same code base.

As part of their SCM functionality, both platforms offer two methods of version control.

1. [Git](https://www.visualstudio.com/team-services/git/) (distributed) - each developer has a copy on their dev machine of the source repository including all branch and history information.

2. Team Foundation Version Control ([TFVC](https://www.visualstudio.com/team-services/tfvc/)), a centralized, client-server system - developers have only one version of each file on their dev machines. Historical data is maintained only on the server.

Microsoft recommends customers use Git for version control unless there is a specific need for centralized version control features. [https://docs.microsoft.com/en-us/vsts/tfvc/comparison-git-tfvc](https://docs.microsoft.com/en-us/vsts/tfvc/comparison-git-tfvc)

This is noteworthy given that in June of 2018 Microsoft purchased [GitHub](../github/), the Internets largest online code repository. This deal closed Oct 26th, 2018.

## Overview
Because the Azure DevOps suite is so wide, similar to GitLab, an overview can be helpfull in understanding what we're dealing with. Go to this [overview page](./azure_devops/overview.html) for more details.

## Strategy
[GitLab strategy with Azure DevOps](https://docs.google.com/presentation/d/1AgPA-2WiHsHdVJABsnYWbBpMHtsi1nVigde1fv9igNU/edit#slide=id.g4a5f174a22_0_13) (GitLab access only)

## Resources
- [Azure DevOps](https://azure.microsoft.com/en-us/services/devops/)
- [Azure DevOps announcement blog](https://azure.microsoft.com/en-us/blog/introducing-azure-devops/)
- [Visual Studio Team Services](https://www.visualstudio.com/team-services/)
- [Team Foundation Server](https://www.visualstudio.com/tfs/))
- [Azure DevOps public roadmap and release history](https://docs.microsoft.com/en-us/azure/devops/release-notes/)
- [Azure DevOps general positioning deck](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=17&cad=rja&uact=8&ved=2ahUKEwjo98_r-bLeAhVlITQIHcPaAqoQFjAQegQIARAC&url=https%3A%2F%2Fwww.pgk.de%2Fpgk%2Fdownloads%2F004-VSTS-TFS_Features_and_Benefits_Deck.pdf&usg=AOvVaw2w3fSaXDirkFgkR4ECxHmg)

## Comments/Anecdotes
* Regarding the rename and repackaging of VSTS to Azure DevOps:
   > Microsoft customers wanted the company to break up the Visual Studio Team Services (VSTS) platform so they could choose individual services, said Jamie Cool, Microsoft's program manager for Azure DevOps. By doing so, the company also hopes to attract a wider audience that includes Mac and Linux developers, as well as open source developers in general, who avoid Visual Studio, Microsoft's flagship development tool set.
* Lots of emphasis on cross platform (windows, Mac, Linux), and free macOS CI/CD is pretty rare.
* All paid plans include unlimited stakeholder users who can view and contribute to work items and boards, and view dashboards, charts, and pipelines
* From [https://azure.microsoft.com/en-us/blog/introducing-azure-devops/](https://azure.microsoft.com/en-us/blog/introducing-azure-devops/)
   > Azure DevOps represents the evolution of Visual Studio Team Services (VSTS). VSTS users will be upgraded into Azure DevOps projects automatically. For existing users, there is no loss of functionally, simply more choice and control. The end to end traceability and integration that has been the hallmark of VSTS is all there. Azure DevOps services work great together.

   > As part of this change, the services have an updated user experience.

   > Users of the on-premises Team Foundation Server (TFS) will continue to receive updates based on features live in Azure DevOps. Starting with next version of TFS, the product will be called Azure DevOps Server and will continue to be enhanced through our normal cadence of updates.
* [HackerNews comments saying it's just a rebrand](https://news.ycombinator.com/item?id=17952273) - PM for AzureDevOps responding:
   > PM for Azure DevOps here (formerly VSTS). It is a rebranding, but it's more than merely a rebranding. We're breaking out the individual services so that they're easier to adopt. For example, if you're just interested in pipelines, you can adopt only pipelines.
* From a call with a prospect Bank:
   - Went with Azure DevOps because
      > It's platform agnostic, it's in the cloud, great capabilityality, tons of functionality, it does what we need it to do. We like it a lot. It really has nothing to do with Microsoft. Microsoft is very agnostic and open source embracing now, so that the old Java vs .Net thing is kind of over.

   - Appealed to a shop that was "more Java than Microsoft technologies". But they had lots of the Microsoft development suite already, and trusted where Microsoft is going.
   - Azure DevOps is dropping new releases every sprint (2-3 weeks). Their roadmap is public: [Azure DevOps public Roadmap and Release History](https://docs.microsoft.com/en-us/azure/devops/release-notes/)
* What does the migration path look like from Azure DevOps to GitLab for SCM and CI/CD??
   * SCM migration - Available tools to migrate without losing repo history. Like with any product, centrally managed -> Git takes training.
   * CI/CD migration - A little tougher. Most existing will be using build pipelines and release pipelines created through UI. Conversion is task by task reconstruction of pipelines, and potential combining into one on GitLab. Azure DevOps pipeline as code is new, although Microsoft is starting to push it as the default now.

## Pricing

### Azure DevOps
{:.no_toc}

* [Azure DevOps Services Pricing](https://azure.microsoft.com/en-us/pricing/details/devops/azure-devops-services/)  
* [Azure Pipelines Only Pricing](https://azure.microsoft.com/en-us/pricing/details/devops/azure-pipelines/)  
* [Azure DevOps On-prem](https://azure.microsoft.com/en-us/pricing/details/devops/on-premises/) = See TFS Pricing
* Current VSTS and MSDN subscribers get different levels of Azure DevOps. Details can be found at [Azure DevOps for Visual Studio subscribers](https://docs.microsoft.com/en-us/visualstudio/subscriptions/vs-azure-devops)

### VSTS
{:.no_toc}

[VSTS Pricing](https://visualstudio.microsoft.com/team-services/pricing/)

Visual Studio ‘Professional Version’ is the most comparable to GitLab since Visual Studio ‘Enterprise Version’ includes extras outside the scope of DevOps (such as MS Office, etc).

Visual Studio Professional can be purchased under a ‘standard’ or ‘cloud’ model.

- Standard = $1,200 year one (retail pricing), then $800 annual renewals (retail pricing)
- Cloud - $540 per year

Under their ‘modern purchasing model’, the monthly cost for Visual Studio Professional (which includes TFS and CAL license) is $45 / mo ($540 / yr).  However, extensions to TFS such as [Test Manager](https://marketplace.visualstudio.com/items?itemName=ms.vss-testmanager-web) ($52/mo), [Package Management](https://marketplace.visualstudio.com/items?itemName=ms.feed) ($15/mo), and [Private Pipelines](https://marketplace.visualstudio.com/items?itemName=ms.build-release-private-pipelines) ($15/mo) require an additional purchase.

### TFS
{:.no_toc}

[TFS Pricing](https://visualstudio.microsoft.com/team-services/tfs-pricing/)

A TFS license can be purchased as standalone product, but a TFS license (and CAL license) is also included when you buy a Visual Studio license / subscription.

MS pushes Visual Studio subscriptions and refers customers who are only interested in a standalone TFS with a ‘classic purchasing’ model to license from a reseller.

Excluding CapEx and Windows operating system license, a standalone TFS license through a reseller in classic purchasing model is approximately $225 per year per instance.  The approximate Client Access License is approximately $320 per year.

## Comparison
