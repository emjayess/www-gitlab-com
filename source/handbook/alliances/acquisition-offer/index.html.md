---
layout: markdown_page
title: "Acquisition Offer"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Acquisition Approach
We are open to acquisitions of companies which are interested in joining in our mission to change all creative work from read-only to read-write so that **everyone can contribute**.
Below are a set of criteria and considerations to take into account as you think about what it might look like joining this mission:

## Considering joining forces with GitLab

1. Please read through our handbook in detail: [/handbook](https://about.gitlab.com/handbook)
1. Remote Culture - spend the time understanding if this for you and your current team. [/company/culture/remote-only](https://about.gitlab.com/culture/remote-only)
1. Did you build a great product but it is missing distribution. We can ensure what you made will be used by more than 100,000 organizations

## Product/Technical considerations
1. Acquired companies have to add something to GitLab which is on our roadmap, see https://about.gitlab.com/direction
1. Your team built a great product but is missing distribution.
1. Willing to reimplement it in GitLab (Ruby and Go) [https://about.gitlab.com/handbook/product/single-application](https://about.gitlab.com/handbook/product/single-application). Willing to sunset old customers with an option to migrate to GitLab. GitLab follows and open-core model: https://about.gitlab.com/install/ce-or-ee
1. Iterate quickly, the best teams that have joined us shipped on the first month (we ship every month on the 22nd).  Eg [Gemnasium](https://about.gitlab.com/2018/01/30/gemnasium-acquired-by-gitlab/)

## What happens to your current company?
1. We acquire assets and not the company entity in order to limit unexpected liabilities
1. Every employee will have a soft landing. We will either make them an offer or we will pay up to a year until they will find a new job.
1. We want to ensure a graceful shutdown - we will pay to shut down the entity including paying outstanding bills up to 2 times the estimated amount.
1. We move fast - close the deal in a week, money wired in 30 days
1. We will consider paying back preferred shareholders up to $1M.
1. We believe talent follows leadership they trust: cash bonus for founders to help in the transition and conditional on employee interviews and offer acceptance
1. Triple our normal stock grants for founders
1. Founders can become distinguished engineers over time
1. Normal cash and stock offers for non-founders

## Post Acquisition
1. Decompression retreat to help the new team reflect on the wild ride of winding down a company and joining a new one.
1. Background from some of our previous acquisitions:  

  **Andrew Newdigate**, “Throughout Gitter's acquisition process, GitLab was an excellent partner. We started the process in mid-December, and by mid-January, the team were onboard and had joined GitLab's summit in Cancún, Mexico. Compared to previous acquisition attempts that we had been through with other companies, the professionalism and focus of the GitLab team was refreshing. It quickly became apparent to me that Sid, Paul and the team at GitLab were interested in striking a fair deal, and were as concerned about the outcome for the Gitter team as my co-founder and I were. In the year-and-half since, GitLab has continued to be a great home for Gitter and I'm incredibly proud to be part of the team.”  

  **Philippe Lafoucrière**, "When GitHub announced they would provide a Security graph and alerts, we knew we would not be able to compete with them. That meant the end of the story for us and our product. It was time to find a new home for the team. We started discussions with GitLab in November 2017 and officially joined the company in mid-January 2018. The whole process was seamless, with a particular care for the team. Expectations from both sides were discussed, with mutual respect and understanding.
The founders made themselves available for us at anytime, and we had regular meetings for several months until we all estimated the situation stable enough. Onboarding a whole team at once is a challenge: the peopleops team did everything they could to help and to make them feel comfortable with their new positions. They were happy to continue on the foundations we’ve built over the years while being able to contribute to a greater goal. We managed to identify with the management where we could have the best impact, and provide results as soon as possible.
I don’t see anything GitLab could have done differently to make this acquisition smoother. The most important value to follow in this kind of event is Trust (because you can only lose it once). GitLab, and especially its CEO, was incredibly clear and respectful during all the process and beyond."

## Acquisition Process
1. Request mutual NDA: [https://about.gitlab.com/handbook/legal](https://about.gitlab.com/handbook/legal)
1. Create issue: to kick start the process, 
    1. Login to GitLab.com. [Start a new issue in the Alliances project with the acquisition-inbound template](https://gitlab.com/gitlab-com/alliances/issues/new?issuable_template=acquisition-inbound). 
    1. Fill in the template with the details, after submitting the issue double check it is marked as ‘confidential’ (the template is set to create confidential issues by default so you don’t need to change anything).
1. Introductory call: the alliances team will review the issue and reach out to schedule a 50 minute introductory call with Brandon Jung, our VP Alliances and/or Eliran Mesika, our Director of Partnerships. The purpose of this is to:
    1. Go over the details shared in the issue
    1. Review the expectations and process noted on this page
    1. Cover any questions the GitLab team has
    1. Answer any questions your team may have
1. Financials: your team will be asked to share the current financial snapshot in the form of a P&L for your company with the alliance contact whom you’ve previously had the introductory call with. Please also include a separate tab which lists the salaries of the entire team. The finance team will then perform an assessment of the company’s financial state.
1. CEO call: If we’re aligned on the details and expectations the alliance owner will reach out to the CEO PA to schedule a 50 minute call. The purpose of this call is to:
    1. Discuss the wind down and onboarding process details including roles for the founders and the placement of the team within GitLab.
    1. Review the employee onboarding goals
1. Technical review: your team will provide access to key contacts at GitLab to your code repository and facilitate access to the products to conduct our technical review.
1. LOI: assuming all terms have been covered and reviewed, the alliances team will share a LOI within 7 days.


## Contact us
For additional information contact Brandon Jung, VP Alliances: bjung@gitlab.com.
Add answers to questions to the page.
