---
layout: markdown_page
title: "E-group KPIs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Intro

Every executive at GitLab has Key Performance Indicators (KPIs).
This page has the goals and links to the definitions.
The goals are merged by the CEO.
The definitions should be in the relevant part of the handbook.
In the definition it should mention what the canonical source is for this indicator.

## CRO

[Looker 45](https://gitlab.looker.com/dashboards/45)

1. IACV vs. plan > 1
1. [Field efficiency ratio](/handbook/finance/operating-metrics/#field-efficiency-ratio) > 2
1. TCV vs. plan > 1
1. Win rate > 30%
1. % of ramped reps at or above quota > 0.7
1. Net Retention > 2
1. Gross Retention > 0.9
1. Rep IACV per comp > 5
1. [ProServe](/handbook/customer-success/implementation-engineering/offerings/) revenue vs. cost > 1.1
1. Services attach rate for strategic > 0.8
1. Self-serve sales ratio > 0.3
1. Licensed users
1. ARPU
1. New strategic accounts

## CMO

[Looker 51](https://gitlab.looker.com/dashboards/51)

1. Pipe generated vs. plan > 1
1. Pipe-to-spend > 5
1. [Marketing efficiency ratio](/handbook/finance/operating-metrics/#marketing-efficiency-ratio) > 2
1. SCLAU
1. CAC / LTV ratio > 4
1. Twitter mentions
1. Sessions on our marketing site
1. New users
1. Installations: Download, start of installation, success installation, created admin user, configured email, second user invited, 30 day active, updates
1. [Social response time](/handbook/marketing/community-relations/community-advocacy/#community-response-channels)
1. Participants at meetups with GitLab presentation
1. GitLab presentations given
1. Wider community contributions per release
1. Monthly Active Contributors from the wider community

## CCO

[Looker 44](https://gitlab.looker.com/dashboards/44)

1. Hires vs. plan > 0.9
1. Apply to hire days < 30
1. No offer NPS > [4.1](https://stripe.com/atlas/guides/scaling-eng)
1. Offer acceptance rate > 0.9
1. Average NPS
1. Average rent index
1. Low rent percentage
1. Monthly turnover
1. YTD turnover
1. Candidates per vacancy
1. Percentage of vacancies with active sourcing
1. New hire average score
1. Onboarding NPS

## CFO

[Looker 43](https://gitlab.looker.com/dashboards/43)

1. IACV per [capital consumed](/handbook/finance/operating-metrics/#capital-consumption) > 2
1. [Sales efficiency](/handbook/finance/operating-metrics/#sales-efficiency-ratio) > 1.0
1. [Magic number](https://about.gitlab.com/handbook/finance/operating-metrics/#magic-number) > 1.1
1. Gross margin > 0.9
1. Average days of sales outstanging < 45
1. Average days to close < 10
1. Runway > 12 months

## VP of Product

[Looker 30](https://gitlab.looker.com/dashboards/30) is not it.

1. SMAU
1. MAU
1. Sessions on release post
1. Installation churn
1. User churn

## VP of Engineering

[Looker 46](https://gitlab.looker.com/dashboards/46)

1. Merge Requests per release
1. Merge Requests per release per developer
1. Uptime GitLab.com
1. Performance GitLab.com
1. Support SLA
1. Support CSAT
1. Support cost vs. recurring revenue
1. Days to fix S1 security issues
1. Days to fix S2 security issues
1. Days to fix S3 security issues
1. GitLab.com infrastructure cost per MAU

## VP of Alliances

[Looker 53](https://gitlab.looker.com/dashboards/53) is broken.

1. Active installations per hosting platform: Total, AWS, Azure, GCP, IBM, Unknown
1. Active users per hosting platform: Total, AWS, Azure, GCP, IBM, Unknown
1. Downloads: Updates, initial
1. Installations: Download, start of installation, success installation, created admin user, configured email, second user invited, 30 day active, updates
1. Distribution method: Omnibus, Cloud native helm chart, Source
