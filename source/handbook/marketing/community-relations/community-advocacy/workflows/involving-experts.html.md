---
layout: markdown_page
title: "Involving experts workflow"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

When responding to community messages, you should always strive to involve a resident GitLab expert on the subject if possible.

This gives:

* a higher quality of answers
* shows that our whole company is committed to helping people
* the expert more feedback from users

## Workflow

Please ping the expert in the relevant channel (e.g. in `#frontend` if it's a frontend question) with:

```plain
@expert_username LINK: [LINK TO COMMUNITY COMMENT]
https://about.gitlab.com/handbook/marketing/community-marketing/community-advocacy/#can-you-please-respond-to-this
Can you please respond to this? Please answer in the social channel that the comment was originally posted in. If you don't know the answer, please share your thoughts and involve someone else, because every remark should get a response.
```

And add an internal note with the link of the Slack message to the associated Zendesk ticket. If there is no Zendesk ticket related to the mention (e.g.a HackerNews mention) track it in the `#community-relations` channel.

## Best practices

When trying to figure out who has expertise on what segment of the product, the handbook Product page has a section called ["Who to talk to for what"](/handbook/product/#who-to-talk-to-for-what). The [team page](/company/team/) can also be useful.

## Automation

TBD: Zendesk
