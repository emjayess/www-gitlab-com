---
layout: markdown_page
title: "GitLab Onboarding Processes"
---

----

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Onboarding Processes

This page is a guide for People Ops when onboarding new team members. All onboarding tasks and guidelines for new team members are in the [People Ops Onboarding Issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md).

PeopleOps needs, at minimum, 3 business days from when the offer/contract is signed by the candidate before they can start. All new hires should be scheduled to start on a Monday whenever possible. This will ensure a good candidate experience and that the new employee has all necessary equipment and proper access prior to starting.

### Git Going at GitLab (Day 1 and Day 2)

We (PeopleOps) are iterating on two orientation calls, called Git Going at GitLab. These are designed to help brand new GitLabbers find their way around in the first few days and covers (for now but more to come) Slack, enabling 2FA, 1Password, working with Google and the Handbook.  These should be arranged on the Monday and Tuesday of the first week

You can find the video of the Day 1 call here:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/JsaEKN9tIYg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Day 2 video can be found here:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/T3Vt9CuwdYQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>




### Onboarding Issue Tasks

- **Select a Buddy**<a name="buddy"></a>
In the "Buddy Spreadsheet" Google sheet, select a buddy for the new team member that is in the same timezone, but a different functional group. Try to pair a technical team member with a non-technical team member and vice versa. Remove the buddy from the spreadsheet, and add the new team member to the sheet so they can also be a buddy one day!
- **Add blank entry to team page**<a name="blank-entry"></a>
Login to [Gitlab.com](www.gitlab.com) and go to the www-gitlab-com project. In
the left menu click "Files," select "data," and choose the file called team.yml. In the top right
corner you can click "edit" to make edits to the code. Scroll all the way down
and copy-paste the code of the previous blank entry to the team page. Edit the
fields to the right info of the new hire and find the right job description URL on
the [Jobs](/jobs/) page. Do not include the employee's name in the commit message, only initials.
 **Note** _This can be tricky, so if you run into trouble reach out to some of
your awesome colleagues in the #questions (channel) on Slack_
- **Invite to swag store**<a name="printfection"></a>
Log in to [Printfection](https://www.printfection.com/). At the top of the home page, click "Campaigns", then scroll down to "Your Favorite Campaigns" and select "New Hire Swag". Go to External Orders and select invite users directly. Invite a new user using their GitLab email and grant them Limited Access. They will need to have this completed before being able to order any swag.
- **Add team member to Beamy**<a name="add-beamy"></a>
Login in to access the settings for the [Beam](https://suitabletech.com/accounts/login/). In the top menu move your cursor over the blue login button. Go to "Manage your beams". Click on "manage" in the lower left corner. Enter the GitLab email and scroll down to find the newly added email. Check the box for "Auto connect".
- **Add team member to Expensify (only with employees)**<a name="add-expensify"></a>
Log in to [Expensify](https://www.expensify.com/signin) and go to "Settings" in the left sidebar. Select the right policy based upon the entity that employs the new team member. Select "People" in the left menu. Select "Invite" and add the GitLab email. Click "invite". If the team member should be added as an admin to be able to also add new team members, update them to a [domain admin](https://docs.expensify.com/advanced-admin-controls/domain-members-and-groups).
- **Add team member to our NexTravel platform**<a name="add-nextravel"></a>
Email the sign-up link which can be found in a secure note (called NexTravel), in the People Ops vault in 1Password. This should be emailed to the team member's GitLab email address.
- **Order business cards**<a name="business-cards"></a>
We send business cards to every team member to make GitLab feel real to people around them despite not having an office. We also expect every team member to be an advocate for GitLab. Go to BambooHR, find the team member, and click on "Business Cards" in the menu. Select "Update Business Card Requests" and fill out all the pertinent fields. Log in with your specific credentials to [Moo](https://www.moo.com/m4b/account/login). Under "Your Account", select "Go to your platform". Click "Create a new product", choose "Business Cards". If the new team member has both a GitLab and a Twitter handle, choose "GitLab 2 - Editable". If the new team member has only one handle, choose "GitLab - Editable". Click "Start Making". Click on the preview to edit the name, job title, email, phone number (formatted `+[country code] number`), social handle(s), and location. Once you have made sure all of the information is correct on the business card, click "Design Backs", "Edit Paper", choose "Super" paper, "Rounded" corners, and how many cards you would like to order (the standard is 50). Click "Review Design", "Save & Finish", and then add the cards to your cart. When you are checking out, update which shipping center you would like the cards sent from at the top right by selecting the proper flag. Enter in the team member's shipping address. Place the order with the applicable shipping and billing addresses.
- **Upgrade team member's Zoom account to Pro**<a name="make-zoom-pro"></a>
1. Log in to the People Ops [Zoom](https://gitlab.zoom.us) account using the credentials stored in 1Password.
1. Click "My Account" in the top right corner.
1. On the left sidebar, under "User Management", click "Users".
1. In the search bar, search for the team member's name (sometimes you may have to search for their email address instead). If the user does not appear yet in Zoom, you can double check with them that they created their Zoom account using their GitLab email address, or you can add them manually by clicking "Add Users" button at the top of the User Management tab.
1. Under "Type", confirm if they have a Pro account. If it says "Basic", click on "Edit" and choose "Pro" and save.
1. If you receive an error, this means we need to purchase more Zoom Pro licenses. This can be done by People Ops by sending an email to our account manager at Zoom, as listed in a shared note on 1Password. The turnaround time is typically 2-3 business days for the new licenses to become activated.
1. Click on their email address.
1. Where it says "Personal Link" click "Customize" and type `gitlab.firstnamelastname` and click "Save Changes". (Please keep in mind that Zoom only accepts the English alphabet.)
1. The Pro account and personal Zoom room is now active!

## Ordering Supplies

If a GitLabber is in need of supplies and is unable to purchase the items themselves, People Ops can place the order, per [Spending Company Money](/handbook/spending-company-money/).

Use the Amazon business account for all Amazon links. In order to see what is available to ship in each country use that country's Amazon website to sign in, place the order, and ship.

For Apple products we have a relationship with Bay Street Business to place all orders and act as our liaison.
* For orders in the US, first check to see if there is a computer that can be shipped from the GitLab boardroom. To track available computers take a look at the shared "Laptop Check-in/Check-out" Google doc. To send a computer to a new team member send an email to the Executive Assistant; include name, shipping information, what type of laptop is needed, and by what date.
* If there is not a computer available for a team member in the US, login to the [custom store](https://idmsa.apple.com/IDMSWebAuth/login.html?appIdKey=a747eeda10429061323c0b5d14a4937fe24fde503ea79cf9cf72a09b0d8f2434&path=/asb2b/init.do%3Fsegment%3DDFL-SEG&language=US-EN&segment=DFL-SEG) to place the order and ship.
* For orders outside the US, but in a country with an Apple Store, please contact Bay Street Business (baystreetbusiness@apple.com) to coordinate with the local store and generate the invoice. Once the invoice is made, it will be sent to People Ops via email. If all information looks correct, contact Bay Street Business with the payment information.

After an order has been placed or a laptop has been shipped, update Finance and BambooHR via [asset tracking](/handbook/finance/asset-tracking/).

## Adding a New Team Member to BambooHR

As part of [onboarding](/handbook/general-onboarding/), People Ops will process new hires in BambooHR. Aside from the steps listed in the onboarding issue, this is a description of how to add the proper information into BambooHR.

Personal Tab

1. Verify the team member was given an Employee ID number.
1. Enter the appropriate Country.
1. Region: Should either be Americas, EMEA, or JAPAC.
1. Verify the work email is entered.

Jobs Tab

1. Hire Date - This will be blank. Make sure to enter in the correct date.
1. Role
   * Leader - if director or above
   * Manager - if has any direct reports
   * Individual Contributor - all others
1. FLSA Code - This will either be exempt or non-exempt depending on how the role is classified. If there are questions on the classification, please ask the People Ops Analyst.
1. Cost Center - Leave blank for now. This will become relevant as we scale.
1. Payroll Type
   * Employee - paid through Payroll
   * Contractor - IND - Independent Contractor agreement
   * Contractor - C2C - Contractor Company agreement
1. Exception to IP Agreement - If they answered Yes, on the IP agreement in the contract. Also, send the text for the exception in an email to the VP of Engineering for approval. File the approved email in BambooHR.
1. Compensation Table
   * Effective Date - Hire Date
   * Pay Rate - Entered as if it were a payroll amount. For example, a US employee would be entered as their yearly amount divided by 24 payrolls in a year. A contractor would have their monthly contract amount listed.
   * Pay Per - Monthly for contractors and employees paid once per month, Pay Period for all other employees
   * Pay Type - Use either Salary or Hourly for employees, or Contract for contractors.
   * Pay Schedule - Select the pay period. Currently we have twice a month for the US, and monthly for all others.
   * Change Reason - Hire
   * Comment - Please add any comments that are relevant from the contract terms.
1. Pay Frequency (Note: Pay Frequency times pay rate should equal annual compensation)
   * 12.96 for GitLab B.V. employees in the Netherlands
   * 13.92 for GitLab B.V. employees in Belgium
   * 24 for GitLab Inc. employees in the United States
   * 12 for everyone else paid monthly
1. On Target Earnings
   * If the team member does not have any variable compensation enter the hire date and Base Only.
   * If the team member does have a variable plan enter the type of bonus, amount or percentage, and annual OTE.
1. Currency Conversion
   * The effective date is either January 1 or July 1, whichever is more recent. Every January and July, the People Ops Analyst will conduct a currency conversion for all team members.
   * Use [Oanda](https://www.oanda.com/currency/converter/) for the currency conversion. Always convert the currency from local currency into USD so that we remain consistent.
   * Enter the currency conversation factor from Oanda with all 5 decimal places.
   * Enter the Local Annual Salary with the appropriate currency code and the converted salary in USD.
1. Job information
   * Effective Date - Hire Date
   * Location - Which entity the new team member is contracted through.
   * Division - Enter the appropriate division from the dropdown.
   * Department - Enter the appropriate department from the dropdown.
   * Job Title - Choose the job title. If it does not exist already, scroll to the bottom, click "Add New" and save the new job title.
   * Reports To - Select their manager.
   * Enter whether the team member is part-time or full-time. Any comments? Add them to the compensation table.
1. Employment Status
   * Enter the hire date and set the status to active. Also leave a comment if there is anything of note in the contract.
   * For new team members from GitLab LTD, Lyra, SafeGuard Spain, and SafeGuard Ireland have a three month probation period.
   * Team members from GitLab GmbH will have a six month probation period.
   Set the status to probation period. This sets up an alert for the manager and People Ops automatically, 2 weeks, 1 week, and a day before the probation period expires. Details of the probation period process can be found on the [contracts page](/handbook/contracts/#probation-period).
1. For employees of HRSavvy, LYRA, and CIIC email the Employee ID number to our contact to align our systems.

Benefits Tab

1. Stock Options
  * Enter the start date
  * Employee or Contractor (This should match Payroll Type on the Jobs Tab)
  * Enter the number of shares from the contract.

### Settings in BambooHR

Changing a Format (Example: Date)

1. Click on Settings
1. Choose Account
1. Select General Settings
1. Change the date format to match desired output

Add a New Division

1. Click on Settings
1. Select Employee Field
1. Select Division
1. Add new division

### Auditing System Changes

At the end of each week a representative from People Operations will review all data entered into BambooHR through the Payroll Change Report for audit purposes. Once a month an audit should be conducted from all payroll providers to ensure the salary information matches BambooHR.
